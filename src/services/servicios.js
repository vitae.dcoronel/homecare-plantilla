import axio from "axios";
/*-URL-RAIZ--------------------------*/
export const url = "https://toolkit.maxialatam.com/bmcqa/";
//export const url = "http://localhost/gcpagil/";
export const servicesBmc = axio.create({
  baseURL: url,
});
export const servicesGcp = axio.create({
  baseURL: "https://toolkit.maxialatam.com/gcpcorrecciones/controller/",
});
/*-SERVICIOS--------------------------*/
export const gcpAgil = (formData) =>
  servicesBmc.post("/index.php", formData, {
    responseType: "json",
  });
export const getBoards = () =>
  servicesBmc.get("/index.php", {
    params: {
      op: "getBoards",
      page: 1,
    },
    responseType: "json",
  });
export const getLists = () =>
  servicesBmc.get("/index.php", {
    params: {
      op: "getlistAll",
      page: 1,
    },
    responseType: "json",
  });
export const getCards = () =>
  servicesBmc.get("/index.php", {
    params: {
      op: "getCardAll",
      page: 1,
    },
    responseType: "json",
  });
