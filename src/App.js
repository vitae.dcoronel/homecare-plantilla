import React, { useEffect } from "react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import store from "./redux/store";
import RootRutas from "./routers/rootRouter";

import "./vendor/bootstrap-select/dist/css/bootstrap-select.min.css";
import "./css/style.css";
import "./App.css";

const App = ({ width, targetRef }) => {
  const body = document.querySelector("body");
  useEffect(() => {
    width >= 1300
      ? body.setAttribute("data-sidebar-style", "full")
      : body.setAttribute("data-sidebar-style", "overlay");
  }, [width, body]);
  return (
    <div ref={targetRef}>
      <Provider store={store}>
        <BrowserRouter>
          <RootRutas width={width} />
        </BrowserRouter>
      </Provider>
    </div>
  );
};
export default App;
