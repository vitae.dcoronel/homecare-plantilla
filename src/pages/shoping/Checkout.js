import React, { Fragment } from "react";
import { useHistory } from "react-router-dom";
const Checkout = () => {
  const History = useHistory();
  const confirm = () => {
    History.push("/mercer/app-detallecompra");
  };
  return (
    <Fragment>
      <div className="row">
        <div className="col-xl-12">
          <div className="card">
            <div className="card-body">
              <div className="row d-flex justify-content-center">
                <div className="col-lg-8 order-lg-2 mb-5">
                  <h4 className="d-flex justify-content-between align-items-center mb-3">
                    <span className="text-muted">Mi compra</span>
                    <span className="badge badge-primary badge-pill">3</span>
                  </h4>
                  <ul className="list-group mb-3">
                    <li className="list-group-item d-flex justify-content-between lh-condensed">
                      <div>
                        <h6 className="my-0">Product name</h6>
                        <small className="text-muted">Brief description</small>
                      </div>
                      <span className="text-muted">$12</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between lh-condensed">
                      <div>
                        <h6 className="my-0">Second product</h6>
                        <small className="text-muted">Brief description</small>
                      </div>
                      <span className="text-muted">$8</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between lh-condensed">
                      <div>
                        <h6 className="my-0">Third item</h6>
                        <small className="text-muted">Brief description</small>
                      </div>
                      <span className="text-muted">$5</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between active">
                      <div className="text-white">
                        <h6 className="my-0 text-white">Promo code</h6>
                        <small>EXAMPLECODE</small>
                      </div>
                      <span className="text-white">-$5</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between">
                      <span>Total (USD)</span>
                      <strong>$20</strong>
                    </li>
                  </ul>

                  <form onSubmit={(e) => e.preventDefault()}>
                    <div className="input-group">
                      <div className="input-group-append">
                        <button
                          type="submit"
                          className="btn btn-primary"
                          onClick={confirm}
                        >
                          Detalle
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Checkout;
