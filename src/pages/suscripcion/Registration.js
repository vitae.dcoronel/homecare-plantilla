import React from "react";
const Registrar = () => {
  return (
    <>
      <div>
        <div className="row d-flex justify-content-center">
          <div className="col-xl-6 col-xxl-6 col-lg-6 ">
            <div className="authincation-content">
              <div className="row no-gutters">
                <div className="col-xl-12">
                  <div className="auth-form">
                    <div className="d-flex flex-column justify-content-center align-items-center">
                      <img
                        src="https://toolkit.maxialatam.com/bmcqa/img/imagenCorporativa.png"
                        alt="https://toolkit.maxialatam.com/bmcqa/img/imagenCorporativa.png"
                        style={{
                          width: "60%",
                          filter: "brightness(1.1)",
                          mixBlendMode: "multiply",
                        }}
                        className="mb-4"
                      ></img>
                      <h4 className="text-center mb-4">Registre su cuenta</h4>
                    </div>
                    <form action="">
                      <div className="form-group">
                        <label className="mb-1">
                          <strong>Nombre Usuario</strong>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Nombre Usuario"
                          name="name"
                        />
                      </div>
                      <div className="form-group">
                        <label className="mb-1">
                          <strong>Email</strong>
                        </label>
                        <input
                          type="email"
                          className="form-control"
                          placeholder="hello@example.com"
                          name="Email"
                        />
                      </div>
                      <div className="form-group">
                        <label className="mb-1">
                          <strong>Clave</strong>
                        </label>
                        <input
                          type="password"
                          className="form-control"
                          defaultValue="Password"
                          name="password"
                        />
                      </div>
                      <div className="text-center mt-4">
                        <input
                          type="submit"
                          value=" Inscríbeme"
                          className="btn btn-primary btn-block"
                        />
                      </div>
                    </form>
                    <div className="new-account mt-3"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Registrar;
