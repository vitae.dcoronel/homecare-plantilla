import React from "react";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import productData from "../productData";

const Dashboard = () => {
  function SampleNextArrow(props) {
    const { onClick } = props;
    return (
      <div className="owl-next" onClick={onClick} style={{ zIndex: 99 }}>
        <i className="fa fa-caret-right" />
      </div>
    );
  }

  function SamplePrevArrow(props) {
    const { onClick } = props;
    return (
      <div
        className="owl-prev disabled"
        onClick={onClick}
        style={{ zIndex: 99 }}
      >
        <i className="fa fa-caret-left" />
      </div>
    );
  }

  const settings = {
    focusOnSelect: true,
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    speed: 500,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 1600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },

      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <>
      <div>
        <div className="row">
          <div className="col-xl-3 col-xxl-6 col-sm-6">
            <div className="card">
              <img
                src="https://i.ytimg.com/vi/1pfQavyaJmg/maxresdefault.jpg"
                alt=""
              ></img>
            </div>
          </div>
          <div className="col-xl-3 col-xxl-6 col-sm-6">
            <div className="card">
              <img
                src="https://s3-us-west-2.amazonaws.com/wp-mpro-blog/wp-content/uploads/2018/10/22032059/Blog-farmacia-01-min.jpg"
                alt=""
              ></img>
            </div>
          </div>
        </div>

        <div
          className=" d-flex justify-content-between mb-3 mb-md-4 align-items-center p-2"
          style={{ background: "#D1D1D1", borderRadius: "0.2rem" }}
        >
          <div className="d-flex align-items-center">
            <div
              style={{
                width: "40px",
                height: "40px",
                borderRadius: "50%",
                background: "#36C95F",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                color: "#ffffff",
              }}
              className="mr-2"
            >
              I
            </div>
            Orden con prescripción
          </div>
          <Button variant="warning">Cargar</Button>
        </div>
        <div className="row">
          <div className="col-xl-12 col-xxl-12 col-lg-12">
            <div className="card">
              <div className="card-header border-0 pb-0">
                <h3 className="fs-20 mb-0 text-black">Compra por categoría</h3>
                <Link to="/reviews" className="text-primary font-w500">
                  View more &gt;&gt;
                </Link>
              </div>
              <div className="card-body">
                <div className="assigned-doctor owl-carousel">
                  <Slider {...settings}>
                    {productData.map((product) => (
                      <div className="items" key={product.key}>
                        <div className="text-center">
                          <img src={product.previewImg} alt="profile" />
                          <div className="dr-star">
                            <i className="las la-star" /> 4.2
                          </div>
                          <h5 className="fs-16 mb-1 font-w600">
                            <Link className="text-black" to="/reviews">
                              {product.title}
                            </Link>
                          </h5>
                          <span className="text-primary mb-2 d-block">
                            Dentist
                          </span>
                          <p className="fs-12">
                            795 Folsom Ave, Suite 600 San Francisco, CADGE 94107
                          </p>
                        </div>
                      </div>
                    ))}
                  </Slider>
                </div>
              </div>
            </div>
          </div>

          <div className="col-xl-12 col-xxl-12 col-lg-12">
            <div className="card">
              <div className="card-header border-0 pb-0">
                <h3 className="fs-20 mb-0 text-black">Ofertas</h3>
                <Link to="/reviews" className="text-primary font-w500">
                  View more &gt;&gt;
                </Link>
              </div>
              <div className="card-body">
                <div className="assigned-doctor owl-carousel">
                  <Slider {...settings}>
                    {productData.map((product) => (
                      <div className="items" key={product.key}>
                        <div className="text-center">
                          <img src={product.previewImg} alt="profile" />
                          <div className="dr-star">
                            <i className="las la-star" /> 4.2
                          </div>
                          <h5 className="fs-16 mb-1 font-w600">
                            <Link className="text-black" to="/reviews">
                              {product.title}
                            </Link>
                          </h5>
                          <span className="text-primary mb-2 d-block">
                            Dentist
                          </span>
                          <p className="fs-12">
                            795 Folsom Ave, Suite 600 San Francisco, CADGE 94107
                          </p>
                        </div>
                      </div>
                    ))}
                  </Slider>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Dashboard;
