import React from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { isNavegacion } from "../../../redux/actions";
const NavHader = () => {
  const dispatch = useDispatch();
  const isNav = useSelector((state) => state.funcionalidad.isNav);
  const toggle = () => {
    dispatch(isNavegacion(!isNav));
  };
  return (
    <div className="nav-header">
      <Link to="/" className="brand-logo">
        <img
          className="logo-abbr"
          src="https://toolkit.maxialatam.com/bmcqa/img/logo.png"
          alt=""
          style={{
            filter: "brightness(1.1)",
            mixBlendMode: "multiply",
          }}
        />
        <img
          className="logo-compact"
          src="https://toolkit.maxialatam.com/bmcqa/img/logo-lg.png"
          alt=""
          style={{
            filter: "brightness(1.1)",
            mixBlendMode: "multiply",
          }}
        />
        <img
          className="brand-title"
          src="https://toolkit.maxialatam.com/bmcqa/img/logo-lg.png"
          alt=""
          style={{
            filter: "brightness(1.1)",
            mixBlendMode: "multiply",
          }}
        />
      </Link>
      <div className="nav-control" onClick={toggle}>
        <div className={`hamburger ${isNav ? "is-active" : ""}`}>
          <span className="line"></span>
          <span className="line"></span>
          <span className="line"></span>
        </div>
      </div>
    </div>
  );
};
export default NavHader;
