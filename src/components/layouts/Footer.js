import React from "react";

const Footer = () => {
  return (
    <div className="footer">
      <div className="copyright">
        <p>Copyright © Designed &amp; Maxia Latam 2021</p>
      </div>
    </div>
  );
};

export default Footer;
