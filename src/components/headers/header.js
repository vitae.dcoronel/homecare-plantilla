import React, { useState, useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { AppContext } from "../../utils";
import { CLEAR_STATE } from "../../actions/actions";
import "./header.css";
function Header() {
  const { dispatch } = useContext(AppContext);
  const history = useHistory();
  const user = useSelector((state) => state.reducerAuth.user);
  const { nombre } = user;
  const [toggle, setToggle] = useState("");
  const onClick = (name) => setToggle(toggle === name ? "" : name);

  const navegacion = (ruta) => {
    dispatch({ action: CLEAR_STATE });
    history.replace(ruta);
  };

  return (
    <nav className="navbar navbar-expand-lg barra">
      <div className="container-fluid">
        <div className="menu">
          <div
            className="nav-item dropdown header-profile "
            onClick={() => onClick("profile")}
          >
            <Link
              className="nav-link"
              to="#"
              role="button"
              data-toggle="dropdown"
            >
              <div className="header-info">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  className="feather feather-menu"
                >
                  <line x1="3" y1="12" x2="21" y2="12"></line>
                  <line x1="3" y1="6" x2="21" y2="6"></line>
                  <line x1="3" y1="18" x2="21" y2="18"></line>
                </svg>
              </div>
            </Link>

            <div
              className={`dropdown-menu dropdown-menu-derecha sombra ${
                toggle === "profile" ? "show" : ""
              }`}
            >
              <Link
                className="dropdown-item ai-icon"
                to="#"
                onClick={(e) => {
                  navegacion("/gcpagil/app-dashboard");
                }}
              >
                <i className="flaticon-381-networking"></i>

                <span className="ml-2">Dashboard</span>
              </Link>

              <Link
                className="dropdown-item ai-icon"
                to="#"
                onClick={(e) => {
                  navegacion("/gcpagil/app-bmc");
                }}
              >
                <i className="flaticon-381-list"></i>
                <span className="ml-2">Bmc</span>
              </Link>
              <Link
                className="dropdown-item ai-icon"
                to="#"
                onClick={(e) => {
                  navegacion("/gcpagil/app-Tableros");
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="34"
                  height="34"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  className="feather feather-trello"
                >
                  <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>
                  <rect x="7" y="7" width="3" height="9"></rect>
                  <rect x="14" y="7" width="3" height="5"></rect>
                </svg>
                <span className="ml-2">Trello</span>
              </Link>
            </div>
          </div>
        </div>

        <div className="">
          <div className="nav-item dropdown header-profile">
            <Link className="nav-link" to="#">
              <div className="header-info">
                <span>
                  <strong>{nombre}</strong>
                </span>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Header;
