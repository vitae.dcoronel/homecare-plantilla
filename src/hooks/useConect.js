import React from "react";

export default function NavigatorOnline(props) {
  // if when navigator.Online change, we can get value. (true or false)
  const updateStatus = () => {
    if (props.onChange) {
      props.onChange(navigator.onLine);
    }
  };

  // windows event listener (online and offline)
  React.useEffect(() => {
    window.addEventListener("online", updateStatus);
    window.addEventListener("offline", updateStatus);
    return () => {
      window.removeEventListener("offline", updateStatus);
      window.removeEventListener("online", updateStatus);
    };
  });

  return null;
}
