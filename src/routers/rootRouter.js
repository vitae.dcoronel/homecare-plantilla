import React, { useEffect, useCallback } from "react";
import { Route, Switch, useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { toast, ToastContainer } from "react-toastify";
//rutas
import { RoutePrivate } from "./HOC/routePrivate";
import PrivateRouter from "./PrivateRouter";
import Login from "../pages/auth/login";
//redux-accion-del-usuario
import { StoreSave } from "../redux/actions";
//hook
import { NavigatorOnline } from "../hooks";
//componentes
import Footer from "../components/layouts/Footer";
import Nav from "../components/layouts/nav";
//css
import "./index.css";
import "./chart.css";
import { infoLocation } from "../helpers/helpers";

const RootRutas = ({ width }) => {
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.reducerAuth.stdAuth);
  const isNav = useSelector((state) => state.funcionalidad.isNav);
  useEffect(() => {
    if (sessionStorage.getItem("session") !== null) {
      let session = sessionStorage.getItem("session");
      let parseado = JSON.parse(session);
      const [{ user, stdAuth }] = parseado;
      dispatch(StoreSave(user, stdAuth));
    }
  }, [dispatch]);

  const offline = useCallback((status) => {
    status
      ? toast.success("Conexion restablecida")
      : toast.error("Intentamos restaurar la conexión");
  }, []);
  //Intentamos restaurar la conexión.Los cambios que haga ahora puede que no guarden
  const { pathname } = useLocation();
  const { path, pagePath } = infoLocation(pathname);

  return (
    <>
      <div
        id={`${!pagePath ? "main-wrapper" : ""}`}
        className={`${!pagePath ? "show" : "mh100vh"} ${
          !path ? "right-profile" : ""
        }${isNav ? "menu-toggle" : ""}`}
      >
        {!pagePath && <Nav width={width} />}

        <div className={`${!pagePath ? "content-body" : ""}`}>
          <div className={`${!pagePath ? "container-fluid" : ""}`}>
            <Switch>
              <Route exact path="/page-login" component={Login} />
              <RoutePrivate
                path="/"
                stdAuth={isAuth}
                component={PrivateRouter}
              />
            </Switch>
          </div>
        </div>

        {!pagePath && <Footer />}
      </div>
      <NavigatorOnline onChange={(status) => offline(status)} />
      <ToastContainer />
    </>
  );
};
export default RootRutas;
