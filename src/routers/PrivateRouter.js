import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Dashboard from "../pages/dashboard/dashboard";
import ProductList from "../pages/productos/ProductList";
import {} from "../pages/productDetails/ProductDetail";
import Checkout from "../pages/shoping/Checkout";
import Register from "../pages/suscripcion/Registration";
import Invoice from "../pages/Invoice/Invoice";
import Maps from "../pages/maps/maps";
//import BoardContainer from "../components/board/BoardContainer";
const PrivateRouter = () => {
  return (
    <>
      <Switch>
        <Route exact path="/mercer/app-inicio" component={Dashboard} />
        <Route exact path="/mercer/app-productos" component={ProductList} />
        <Route exact path="/mercer/app-productos" component={ProductList} />
        <Route exact path="/mercer/app-micompra" component={Checkout} />
        <Route exact path="/mercer/app-registrar" component={Register} />
        <Route exact path="/mercer/app-detallecompra" component={Invoice} />
        <Route exact path="/mercer/app-seguimiento" component={Maps} />
        {/* <Route path="/gcpagil/page-b/:boardId" component={BoardContainer} />
         */}
        <Route
          exact
          path="/"
          render={() => <Redirect to="/mercer/app-Inicio" />}
        />
      </Switch>
    </>
  );
};
export default PrivateRouter;
