import { gcpAgil } from "../../services/servicios";
import { changeList } from "./listById";
import { toast } from "react-toastify";
export const ADD_CARD = "[CARDS] ADD_CARD";
export const CHANGE_CARD_TITLE = "[CARDS] CHANGE_CARD_TITLE";
export const CHANGE_CARD_DESCP = "[CARDS] CHANGE_CARD_DESCP";
export const CHANGE_CARD_DATE_START = "[CARDS] CHANGE_CARD_DATE_START";
export const CHANGE_CARD_DATE_END = "[CARDS] CHANGE_CARD_DATE_END";
export const CHANGE_CARD_COLOR = "[CARDS] CHANGE_CARD_COLOR";
export const CHANGE_CARD_ADD_RESP = "[CARDS] CHANGE_CARD_ADD_RESP";
export const CHANGE_CARD_DELET_RESP = "[CARDS] CHANGE_CARD_DELET_RESP";
export const DELETE_CARD = "[CARDS] DELETE_CARD";
export const HYDRATE_STATE_CARDS = "[CARDS] HYDRATE_STATE_CARDS";
export const DELETE_LIST = "[CARDS] DELETE_LIST";
export const COMENTARIO_NUMBER = "[CARDS] COMENTARIO_NUMBER";
export const ADJUNTO_NUMBER = "[CARDS] ADJUNTO_NUMBER";
export const IMG_PORTADA = "[CARDS] ADD_IMG_PORTADA";
export const IS_PORTADA = "[CARDS] ADD_IMG_PORTADA";
export const CHECK_TOTAL = "[CARDS] CHECK_TOTAL";
export const CHECK_AVANCE = "[CARDS] CHECK_AVANCE";

export const saveCarddb = (state, idlist) => {
  return (dispatch) => {
    const { _id, title } = state;
    console.log({ _id: _id, title: title });
    const content = JSON.stringify(state);
    let formDa = new FormData();
    formDa.append("op", "addCard");
    formDa.append("content", content);
    formDa.append("id", idlist);
    gcpAgil(formDa)
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.rsp === 1) {
            dispatch(cardAdd(_id, title));
            dispatch(changeList("changeList", idlist, "addCard", _id, ""));
          } else if (response.data.rsp === 0) {
            toast.warn("Error al agregada la tarjeta");
          }
        }
      })
      .catch(function (error) {
        console.error("save boards" + error);
      });
  };
};
//UPDATE lists SET contenido = JSON_ARRAY_APPEND(contenido,'$.cards','1616787551129') WHERE id ='125'
export const changeCard = (op, id, change, data, idopcional = "") => {
  return (dispatch) => {
    console.log({
      op: op,
      id: id,
      change: change,
      data: data,
      idopcional: idopcional,
    });
    let formDa = new FormData();
    formDa.append("op", op);
    formDa.append("id", id);
    formDa.append("change", change);
    formDa.append("data", data);
    gcpAgil(formDa)
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.estatus === "ok") {
            if (response.data.change === "title") {
              dispatch(cardChangeTitle(id, data));
            } else if (response.data.change === "description") {
              dispatch(cardChangeDescrip(id, data));
            } else if (response.data.change === "dateStart") {
              dispatch(cardChangeDateStart(id, data));
            } else if (response.data.change === "dateEnd") {
              dispatch(cardChangeDateEnd(id, data));
            } else if (response.data.change === "color") {
              dispatch(cardChangeColor(id, data));
            } else if (response.data.change === "addResponsable") {
              dispatch(cardChangeAddResp(id, data));
            } else if (response.data.change === "deletResponsable") {
              dispatch(cardChangeDeletResp(id, idopcional));
            } else if (response.data.change === "portada") {
              dispatch(cardPortadaAdd(id, data));
            } else {
              dispatch(cardDelet(id));
            }
          } else {
            toast.warn("no se logro actualizar");
          }
        }
      })
      .catch(function (error) {
        console.error("error al actualizar los cards" + error);
      });
  };
};
export const CardVencimiento = (id, change, data, dataD) => {
  return (dispatch) => {
    console.log({
      id: id,
      data: data,
      dataD: dataD,
    });
    let formDa = new FormData();
    formDa.append("op", "CardVencimiento");
    formDa.append("id", id);
    formDa.append("Finicio", data);
    formDa.append("Fvencimiento", dataD);
    formDa.append("change", change); //cambiar
    gcpAgil(formDa)
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.estatus === "ok") {
            if (response.data.change === "addfechas") {
              dispatch(cardChangeDateStart(id, data));
              dispatch(cardChangeDateEnd(id, dataD));
            } else {
              dispatch(cardChangeDateStart(id, ""));
              dispatch(cardChangeDateEnd(id, ""));
            }
          }
        }
      })
      .catch(function (error) {
        console.error("error al actualizar vencimiento" + error);
      });
  };
};
export const incrementar = (cardId, opcion, change) => {
  return (dispatch, state) => {
    const { cardsById } = state();
    let adjuntosActual = cardsById[cardId].adjuntos;
    let comentarioActual = cardsById[cardId].comentario;
    let total = 0;
    if (change === "numeroComentarios") {
      total =
        opcion === "incrementar" ? comentarioActual + 1 : comentarioActual - 1;
    } else if (change === "numeroAdjunto") {
      total =
        opcion === "incrementar" ? adjuntosActual + 1 : adjuntosActual - 1;
    }
    let totalCero = total === 0 ? "cero" : total;
    console.log({
      cardId: cardId,
      opcion: opcion,
      change: change,
      total: total,
    });
    let formDa = new FormData();
    formDa.append("op", change);
    formDa.append("id", cardId);
    formDa.append("data", totalCero);
    gcpAgil(formDa)
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.estatus === "ok") {
            if (response.data.change === "adjunto") {
              //dispatch(cardNumberAdjunto(cardId, total));
            } else {
              //dispatch(cardNumberComentatio(cardId, total));
            }
          }
        }
      })
      .catch(function (error) {
        console.error("error al actualizar cantidad" + error);
      });
  };
};
export const checkControl = (cardId, opcion, change) => {
  return (dispatch, state) => {
    const { cardsById } = state();
    let checkTotal = cardsById[cardId].chekList.total;
    let checkAvance = cardsById[cardId].chekList.avance;
    console.log({ checkTotal, checkAvance, opcion });
    let total = 0;
    if (change === "checkListTotal") {
      total = opcion === "incrementar" ? checkTotal + 1 : checkTotal - 1;
    } else {
      total = opcion === "incrementar" ? checkAvance + 1 : checkAvance - 1;
    }
    let totalCero = total === 0 ? "cero" : total;
    console.log({
      cardId: cardId,
      opcion: opcion,
      change: change,
      total: total,
    });
    let formDa = new FormData();
    formDa.append("op", "estadisticaCheckList");
    formDa.append("id", cardId);
    formDa.append("change", change);
    formDa.append("data", totalCero);
    gcpAgil(formDa)
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.estatus === "ok") {
            if (response.data.change === "checkListTotal") {
              dispatch(cardCheckTotal(cardId, total));
            } else {
              dispatch(cardCheckAvance(cardId, total));
            }
          }
        }
      })
      .catch(function (error) {
        console.error("error al actualizar cantidad" + error);
      });
  };
};
export const injectStateDbCards = (listId, contenido) => ({
  type: HYDRATE_STATE_CARDS,
  payload: {
    contenido: JSON.parse(contenido),
    cardId: JSON.parse(listId),
  },
});
export const cardAdd = (cardId, title) => ({
  type: ADD_CARD,
  payload: { cardId, title },
});
export const cardChangeTitle = (cardId, data) => ({
  type: CHANGE_CARD_TITLE,
  payload: { data, cardId },
});
export const cardChangeDescrip = (cardId, data) => ({
  type: CHANGE_CARD_DESCP,
  payload: { data, cardId },
});
export const cardChangeDateStart = (cardId, data) => ({
  type: CHANGE_CARD_DATE_START,
  payload: { data, cardId },
});
export const cardChangeDateEnd = (cardId, data) => ({
  type: CHANGE_CARD_DATE_END,
  payload: { data, cardId },
});
export const cardChangeColor = (cardId, data) => ({
  type: CHANGE_CARD_COLOR,
  payload: { data, cardId },
});
export const cardChangeAddResp = (cardId, data) => ({
  type: CHANGE_CARD_ADD_RESP,
  payload: { data, cardId },
});
export const cardChangeDeletResp = (cardId, data) => ({
  type: CHANGE_CARD_DELET_RESP,
  payload: { data, cardId },
});
//DELETE_CARD
export const cardDelet = (cardId) => ({
  type: DELETE_CARD,
  payload: { cardId },
});
export const cardNumberComentatio = (cardId, data) => ({
  type: COMENTARIO_NUMBER,
  payload: { data, cardId },
});
export const cardNumberAdjunto = (cardId, data) => ({
  type: ADJUNTO_NUMBER,
  payload: { data, cardId },
});
export const cardPortadaAdd = (cardId, data) => ({
  type: IMG_PORTADA,
  payload: { data, cardId },
});
export const cardIsPortada = (cardId, data) => ({
  type: IS_PORTADA,
  payload: { data, cardId },
});
export const cardCheckTotal = (cardId, data) => ({
  type: CHECK_TOTAL,
  payload: { data, cardId },
});
export const cardCheckAvance = (cardId, data) => ({
  type: CHECK_AVANCE,
  payload: { data, cardId },
});
/*
const convertArrayToObject = (array, key) =>
  array.reduce(
    (obj, item) => ({
      ...obj,
      [item[key]]: item
    }),
    {}
  ); */
