/*-TABLEROS-----------------------------------------------------*/
export const CURRENT_BOARD_ID = "[FUNCIONALIDAD] CURRENT_BOARD_ID";
export const STATUS_BOARD_ADD = "[FUNCIONALIDAD] STATUS_BOARD_ADD";
/*-LISTAS------------------------------------------------------*/
export const CURRENT_LISTS_ID = "[FUNCIONALIDAD] CURRENT_LISTS_ID";
export const STATUS_LIST_ADD = "[FUNCIONALIDAD] STATUS_LIST_ADD";
/*-TARJETAS-----------------------------------------------------*/
export const CURRENT_CARDS_ID = "[FUNCIONALIDAD] CURRENT_CARDS_ID";
export const MODAL_FORM_CARDS = "[FUNCIONALIDAD] MODAL_FORM_CARDS";
/*-BMC----------------------------------------------------------*/
export const MODAL_FORM_EMPRESAS = "[FUNCIONALIDAD] MODAL_FORM_EMPRESAS";
export const TIPO_ACCION_USUARIO = "[FUNCIONALIDAD] TIPO_ACCION_USUARIO";
/*-FUNCIONALIDAD-MENU--*/
export const ISMENU = "[FUNCIONALIDAD] ISMENU";
export const currentBoardId = (boardId) => ({
  type: CURRENT_BOARD_ID,
  payload: { boardId },
});
export const statusBoardAdd = (statusBoard) => ({
  type: STATUS_BOARD_ADD,
  payload: { statusBoard },
});
export const currentListsId = (listId) => ({
  type: CURRENT_LISTS_ID,
  payload: { listId },
});
export const statusListAdd = (statusList) => ({
  type: STATUS_LIST_ADD,
  payload: { statusList },
});
export const currentCardsId = (cardId) => ({
  type: CURRENT_CARDS_ID,
  payload: { cardId },
});
export const isModalFomrCards = (ismodal) => ({
  type: MODAL_FORM_CARDS,
  payload: { ismodal },
});
/*-BMC-------------------------------------*/
export const isModalFomrEmpresas = (ismodal) => ({
  type: MODAL_FORM_EMPRESAS,
  payload: { ismodal },
});
//FUNCIONALIDAD
export const tipo_accion_usuario = (user, accion) => ({
  type: TIPO_ACCION_USUARIO,
  payload: {
    userProyect: user,
    accion: accion,
  },
});

export const isNavegacion = (data) => ({
  type: ISMENU,
  payload: {
    isnav: data,
  },
});
