import { toast } from "react-toastify";
import { gcpAgil } from "../../services/servicios";
import { changeBoard } from "./board";
import { changeCard } from "./cards";
import { statusListAdd } from "./funcionalidad";
export const ADD_LIST = "[list] ADD_LIST";
export const CHANGE_LIST_TITLE = "[list] CHANGE_LIST_TITLE";
export const HYDRATE_STATE_LIST = "[list] HYDRATE_STATE_LIST";
export const DELETE_LIST = "[list] DELETE_LIST";
export const MOVE_CARD = "[list] MOVE_CARD";
export const ADD_CARD = "[list] ADD_CARD";
export const DELETE_CARD = "[list] DELETE_CARD";
export const STATE_INJECTION = "[list] STATE_INJECTION";

export const saveListdb = (boardId, state) => {
  return (dispatch) => {
    console.log({ boardId: boardId, state: state });
    const { _id, title } = state;
    const list = JSON.stringify(state);
    let formDa = new FormData();
    formDa.append("op", "addlist");
    formDa.append("id", boardId);
    formDa.append("content", list);
    gcpAgil(formDa)
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.estado === "ok") {
            dispatch(listAdd(_id, title));
            dispatch(changeBoard("changeBoards", boardId, "addList", _id));
            dispatch(statusListAdd(true));
            toast.success("Lista agregada exitosamente");
          } else {
            dispatch(statusListAdd(false));
            toast.warning("Error al agregar lista");
          }
        }
      })
      .catch(function (error) {
        console.error("insert list" + error);
      });
  };
};
export const changeList = (op, id, change, data, idCard, IdBoard) => {
  return (dispatch) => {
    console.log({
      op: op,
      id: id,
      change: change,
      data: data,
      idCard: idCard,
      IdBoard: IdBoard,
    });
    let formDa = new FormData();
    formDa.append("op", op);
    formDa.append("id", id);
    formDa.append("data", data);
    formDa.append("change", change);
    gcpAgil(formDa)
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.estado === "ok") {
            if (response.data.change === "title") {
              dispatch(changeListTitle(data, id));
            } else if (response.data.change === "addCard") {
              dispatch(listAddCard(id, data)); //idopcional =idcard
            } else if (response.data.change === "deletCard") {
              dispatch(listDeletCard(id, idCard));
              dispatch(changeCard("deletCard", idCard, "", IdBoard, ""));
            } else if (response.data.change === "deletlist") {
              dispatch(ListDelet(data)); //idopcional =idlist
            } else {
            }
          } else {
            toast.error("operacion fallida");
          }
        }
      })
      .catch(function (error) {
        console.error("list" + error);
      });
  };
};

export const injectStateDbList = (listId, contenido) => ({
  type: HYDRATE_STATE_LIST,
  payload: {
    contenido: JSON.parse(contenido),
    listId: JSON.parse(listId),
  },
});

export const dragAndDrogCard = (
  oldCardIndex,
  newCardIndex,
  sourceListId,
  destListId
) => {
  return (dispatch, state) => {
    /*-ACCION-SINCRONA-ACTUALIZA-STATE- -------------------------*/
    dispatch(
      listMoveCard(oldCardIndex, newCardIndex, sourceListId, destListId)
    );
    const { listsById } = state();
    let IdCardMovida = listsById[destListId].cards[newCardIndex];
    console.log({
      IdlistDestino: destListId,
      IdCardMovida: IdCardMovida,
    });
    //console.log(oldCardIndex, newCardIndex, sourceListId, destListId);
    if (sourceListId !== destListId) {
      console.log(`Deplazamiento de card de una lista a otra`);
      dispatch(
        infoCard(
          sourceListId,
          destListId,
          "betweenList",
          destListId,
          IdCardMovida
        )
      );
    } else if (oldCardIndex !== newCardIndex) {
      console.log("desplazamiento de card de arriba a abajo o contrario");
      dispatch(infoCard(sourceListId, destListId, "cards", "", ""));
    }
  };
};

export const infoCard = (
  idlistA,
  idlistB,
  actionType,
  idListDestino,
  idCardMovida
) => {
  return (dispatch, state) => {
    const { listsById } = state();
    /*-CONTENIDO DE LA LISTAS A MODIFICAR- */
    let x = listsById[idlistA];
    let y = listsById[idlistB];
    /*-CONVIRTIENDO-A-JSON- */
    const listx = JSON.stringify(x);
    const listy = JSON.stringify(y);
    /*-ARRAY-PARA-ENVIAR-PETICION-*/
    let lst = [];
    if (actionType === "betweenList") {
      lst.push({ id: idlistA, data: listx });
      lst.push({ id: idlistB, data: listy });
      let data = JSON.stringify(lst);
      dispatch(dragDrog("listDragAnDrog", data, idListDestino, idCardMovida));
    } else {
      lst.push({ id: idlistA, data: listx });
      let data = JSON.stringify(lst);
      dispatch(dragDrog("listDragAnDrog", data, "", ""));
    }
  };
};

export const dragDrog = (op, data, idListDestino, idCardMovida) => {
  return (dispatch) => {
    let formDa = new FormData();
    formDa.append("op", op);
    formDa.append("data", data);
    formDa.append("idListDestino", idListDestino);
    formDa.append("idCardMovida", idCardMovida);
    gcpAgil(formDa)
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.estatus === "ok") {
          } else {
            toast.error("operacion fallida");
          }
        }
      })
      .catch(function (error) {
        console.error("list" + error);
      });
  };
};

export const listAdd = (listId, listTitle) => ({
  type: ADD_LIST,
  payload: { listTitle, listId },
});

export const changeListTitle = (newTitle, listId) => ({
  type: CHANGE_LIST_TITLE,
  payload: { listTitle: newTitle, listId },
});
export const ListDelet = (listId) => ({
  type: DELETE_LIST,
  payload: { listId },
});

export const listAddCard = (listId, cardId) => ({
  type: ADD_CARD,
  payload: { listId, cardId },
});

export const listMoveCard = (
  oldCardIndex,
  newCardIndex,
  sourceListId,
  destListId
) => ({
  type: MOVE_CARD,
  payload: { oldCardIndex, newCardIndex, sourceListId, destListId },
});

export const listDeletCard = (listId, cardId) => ({
  type: DELETE_CARD,
  payload: { cardId, listId },
});

export const state_injection_list = (data) => ({
  type: STATE_INJECTION,
  payload: data,
});
