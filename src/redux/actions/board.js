import { gcpAgil } from "../../services/servicios";
import { changeList } from "./listById";
import { currentBoardId, statusBoardAdd } from "./funcionalidad";
import { toast } from "react-toastify";
export const ADD_BOARD = "[BOARD] ADD_BOARD";
export const CHANGE_BOARD_TITLE = "[BOARD] CHANGE_BOARD_TITLE";
export const CHANGE_BOARD_COLOR = "[BOARD] CHANGE_BOARD_COLOR";
export const DELETE_BOARD = "[BOARD] DELETE_BOARD";
export const UPDATE_STATE_DATA = "[BOARD] UPDATE_STATE_DATA";
export const ADD_LIST = "[BOARD] ADD_LIST";
export const MOVE_LIST = "[BOARD] MOVE_LIST";
export const DELETE_LIST = "[BOARD] DELETE_LIST";
export const STATE_INJECTION = "[BOARD]STATE_INJECTION";
export const savedb = (title, state, usuario) => {
  return (dispatch) => {
    let titulo = title ? title : "titulo por defecto";
    const structura = JSON.stringify(state);
    let formDa = new FormData();
    formDa.append("op", "addBoards");
    formDa.append("titulo", titulo);
    formDa.append("content", structura);
    formDa.append("usuario", usuario);
    gcpAgil(formDa)
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.rsp === 1) {
            dispatch(boardAdd(title, response.data.id.toString(), usuario));
            dispatch(currentBoardId(response.data.id));
            dispatch(statusBoardAdd(true));
            toast.success("Tablero agregada exitosamente");
          } else if (response.data.rsp === 0) {
            toast.warn("Error al agregada tablero");
            dispatch(statusBoardAdd(false));
          }
        }
      })
      .catch(function (error) {
        console.error("save boards" + error);
        toast.error("Ocurrio un problema");
      });
  };
};
//("changeBoards", boardId, "deletList", indiceList, listId)
export const changeBoard = (op, id, change, data, idlist = "") => {
  return (dispatch) => {
    console.log({ op: op, id: id, change: change, data: data, idlist: idlist });
    let formDa = new FormData();
    formDa.append("op", op);
    formDa.append("id", id);
    formDa.append("change", change); //cambiar
    formDa.append("data", data);
    gcpAgil(formDa)
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.estado === "ok") {
            if (response.data.change === "title") {
              dispatch(boardChangeTitle(id, data));
            } else if (response.data.change === "color") {
              dispatch(boardChangeColor(id, data));
            } else if (response.data.change === "addList") {
              dispatch(boardAddList(id, data));
            } else if (response.data.change === "deletList") {
              dispatch(boardDeletList(idlist, id));
              dispatch(changeList("deletlist", id, "delet", idlist, ""));
            } else if (response.data.change === "addUser") {
              console.log("adduser");
            } else if (response.data.change === "deletUser") {
              console.log("adduser");
            } else {
              console.log("tablero eliminado");
              dispatch(boardDelet(id));
            }
          } else {
            console.warn("no se logro actualizar");
          }
        }
      })
      .catch(function (error) {
        console.error("udpate id boards" + error);
      });
  };
};

export const move = (oldListIndex, newListIndex, boardId) => {
  return (dispatch, state) => {
    const { boardsById } = state();
    let x = boardsById[boardId];
    dispatch(boardMoveList(oldListIndex, newListIndex, boardId));
    dispatch(info(x, boardId));
  };
};

export const info = (oldstate, boardId) => {
  return (dispatch, state) => {
    const { boardsById } = state();
    let x = boardsById[boardId];
    //console.log(x);
    const board = JSON.stringify(x);
    //console.log(board, boardId);
    dispatch(changeBoard("boardDragAnDrog", boardId, "DragAnDrog", board, ""));
  };
};
export const injectStateDb = (boardId, contenido) => ({
  type: UPDATE_STATE_DATA,
  payload: {
    contenido: JSON.parse(contenido),
    boardId: JSON.parse(boardId),
  },
});
export const boardAdd = (title, boardId, userId) => ({
  type: ADD_BOARD,
  payload: {
    boardTitle: title,
    boardId,
    userId,
  },
});

export const boardDelet = (boardId) => ({
  type: DELETE_BOARD,
  payload: { boardId },
});

export const boardChangeTitle = (boardId, newTitle) => ({
  type: CHANGE_BOARD_TITLE,
  payload: {
    boardTitle: newTitle,
    boardId,
  },
});

export const boardChangeColor = (boardId, color) => ({
  type: CHANGE_BOARD_COLOR,
  payload: { boardId, color },
});

export const boardAddList = (boardId, listId) => ({
  type: ADD_LIST,
  payload: { boardId, listId },
});

export const boardMoveList = (oldListIndex, newListIndex, boardId) => ({
  type: MOVE_LIST,
  payload: { oldListIndex, newListIndex, boardId },
});

export const boardDeletList = (newListId, boardId) => ({
  type: DELETE_LIST,
  payload: { listId: newListId, boardId },
});

export const state_injection = (data) => ({
  type: STATE_INJECTION,
  payload: data,
});
