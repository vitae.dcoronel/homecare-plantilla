import {
  ADD_CARD,
  CHANGE_CARD_TITLE,
  CHANGE_CARD_DESCP,
  CHANGE_CARD_DATE_START,
  CHANGE_CARD_DATE_END,
  CHANGE_CARD_ADD_RESP,
  CHANGE_CARD_DELET_RESP,
  CHANGE_CARD_COLOR,
  DELETE_CARD,
  DELETE_LIST,
  HYDRATE_STATE_CARDS,
  IMG_PORTADA,
  ADJUNTO_NUMBER,
  COMENTARIO_NUMBER,
  CHECK_TOTAL,
  CHECK_AVANCE,
} from "../actions/cards";
const cardsById = (state = {}, action) => {
  switch (action.type) {
    case ADD_CARD: {
      const { cardId, title } = action.payload;
      return {
        ...state,
        [cardId]: {
          _id: cardId,
          title: title,
          description: "",
          dateStart: "",
          dateEnd: "",
          color: "#fff",
          imgPortada: { img: "", isActiva: "inactiva" }, //activa/inactiva
          comentario: 0,
          adjuntos: 0,
          fechas: { inicial: "", vencimiento: "", estado: "inactiva" },
          chekList: { list: [], total: 0, avance: 0 },
          etiquetas: [],
          responsable: [],
        },
      };
    }
    case CHANGE_CARD_TITLE: {
      const { data, cardId } = action.payload;

      return { ...state, [cardId]: { ...state[cardId], title: data } };
    }
    case CHANGE_CARD_DESCP: {
      const { data, cardId } = action.payload;
      return { ...state, [cardId]: { ...state[cardId], description: data } };
    }
    case CHANGE_CARD_DATE_START: {
      const { data, cardId } = action.payload;
      return { ...state, [cardId]: { ...state[cardId], dateStart: data } };
    }
    case CHANGE_CARD_DATE_END: {
      const { data, cardId } = action.payload;
      return { ...state, [cardId]: { ...state[cardId], dateEnd: data } };
    }
    case CHANGE_CARD_COLOR: {
      const { data, cardId } = action.payload;
      return { ...state, [cardId]: { ...state[cardId], color: data } };
    }
    case ADJUNTO_NUMBER: {
      const { data, cardId } = action.payload;
      return { ...state, [cardId]: { ...state[cardId], adjuntos: data } };
    }
    case COMENTARIO_NUMBER: {
      const { data, cardId } = action.payload;
      return { ...state, [cardId]: { ...state[cardId], comentario: data } };
    }
    case CHANGE_CARD_ADD_RESP: {
      const { data, cardId } = action.payload;
      return {
        ...state,
        [cardId]: {
          ...state[cardId],
          responsable: [...state[cardId].responsable, data],
        },
      };
    }
    case CHANGE_CARD_DELET_RESP: {
      const { data: DeletResp, cardId } = action.payload;
      return {
        ...state,
        [cardId]: {
          ...state[cardId],
          responsable: state[cardId].responsable.filter(
            (resp) => resp !== DeletResp
          ),
        },
      };
    }
    case IMG_PORTADA: {
      const { data, cardId } = action.payload;
      return {
        ...state,
        [cardId]: {
          ...state[cardId],
          imgPortada: {
            ...state[cardId].imgPortada,
            img: data,
            isActiva: "activa",
          },
        },
      };
    }
    case CHECK_TOTAL: {
      const { data, cardId } = action.payload;
      return {
        ...state,
        [cardId]: {
          ...state[cardId],
          chekList: {
            ...state[cardId].chekList,
            total: data,
          },
        },
      };
    }
    case CHECK_AVANCE: {
      const { data, cardId } = action.payload;
      return {
        ...state,
        [cardId]: {
          ...state[cardId],
          chekList: {
            ...state[cardId].chekList,
            avance: data,
          },
        },
      };
    }
    case DELETE_CARD: {
      const { cardId } = action.payload;
      const { [cardId]: deletedCard, ...restOfCards } = state;
      return restOfCards;
    }
    // Find every card from the deleted list and remove it (actually unnecessary since they will be removed from db on next write anyway)
    case DELETE_LIST: {
      const { cards: cardIds } = action.payload;
      return Object.keys(state)
        .filter((cardId) => !cardIds.includes(cardId))
        .reduce(
          (newState, cardId) => ({ ...newState, [cardId]: state[cardId] }),
          {}
        );
    }
    case HYDRATE_STATE_CARDS: {
      const { cardId, contenido } = action.payload;
      return {
        ...state,
        [cardId]: contenido,
      };
    }
    default:
      return state;
  }
};
export default cardsById;
/*
chekList.total;
chekList.porcentaje;

UPDATE cards SET contenido = JSON_REMOVE(contenido,'$.chekList.porcentaje') WHERE id = 1;

UPDATE cards SET contenido =JSON_SET(contenido,'$.chekList.total',0) WHERE id =*/
