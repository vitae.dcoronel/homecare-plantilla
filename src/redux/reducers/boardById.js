import {
  ADD_LIST,
  MOVE_LIST,
  DELETE_LIST,
  ADD_BOARD,
  CHANGE_BOARD_TITLE,
  CHANGE_BOARD_COLOR,
  DELETE_BOARD,
  UPDATE_STATE_DATA,
  STATE_INJECTION,
} from "../actions/board";

const boardsById = (state = {}, action) => {
  switch (action.type) {
    case ADD_LIST: {
      const { boardId, listId } = action.payload;
      return {
        ...state,
        [boardId]: {
          ...state[boardId],
          lists: [...state[boardId].lists, listId],
        },
      };
    }
    case MOVE_LIST: {
      const { oldListIndex, newListIndex, boardId } = action.payload;
      const newLists = Array.from(state[boardId].lists);
      const [removedList] = newLists.splice(oldListIndex, 1);
      newLists.splice(newListIndex, 0, removedList);
      return {
        ...state,
        [boardId]: { ...state[boardId], lists: newLists },
      };
    }
    case DELETE_LIST: {
      const { listId: newListId, boardId } = action.payload;
      const listFrm = newListId.toString();
      return {
        ...state,
        [boardId]: {
          ...state[boardId],
          lists: state[boardId].lists.filter((listId) => listId !== listFrm),
        },
      };
    }
    case ADD_BOARD: {
      const { boardTitle, boardId, userId } = action.payload;
      return {
        ...state,
        [boardId]: {
          _id: boardId,
          title: boardTitle,
          lists: [],
          users: [userId],
          color: "gray", //#F6F8FA #E4E9EA
        },
      };
    }
    case CHANGE_BOARD_TITLE: {
      const { boardTitle, boardId } = action.payload;
      return {
        ...state,
        [boardId]: {
          ...state[boardId],
          title: boardTitle,
        },
      };
    }
    case CHANGE_BOARD_COLOR: {
      const { boardId, color } = action.payload;
      return {
        ...state,
        [boardId]: {
          ...state[boardId],
          color,
        },
      };
    }
    case DELETE_BOARD: {
      const { boardId } = action.payload;
      const { [boardId]: deletedBoard, ...restOfBoards } = state;
      return restOfBoards;
    }
    case UPDATE_STATE_DATA: {
      const { boardId, contenido } = action.payload;
      return {
        ...state,
        [boardId]: contenido,
      };
    }

    case STATE_INJECTION:
      try {
        console.log(action.payload);
        state = action.payload;
        console.log(state);
      } catch (ex) {}
      return state;
    default:
      return state;
  }
};

export default boardsById;
/*{
          _id: contenido._id,
          title: contenido.title,
          lists: JSON.parse(contenido.lists),
          users: contenido.users,
          color: contenido.color,
        } */
