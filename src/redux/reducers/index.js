import { combineReducers } from "redux";
import reducerAuth from "./Auth";
import boardsById from "./boardById";
import listsById from "./listsById";
import cardsById from "./cardsById";
import funcionalidad from "./funcionalidad";
export default combineReducers({
  reducerAuth,
  boardsById,
  listsById,
  cardsById,
  funcionalidad,
});
