import {
  CURRENT_BOARD_ID,
  STATUS_BOARD_ADD,
  CURRENT_LISTS_ID,
  STATUS_LIST_ADD,
  CURRENT_CARDS_ID,
  MODAL_FORM_CARDS,
  MODAL_FORM_EMPRESAS,
  TIPO_ACCION_USUARIO,
  ISMENU,
} from "../actions/funcionalidad";
const estado = {
  currentIdboard: null,
  statusBoardAdd: false,
  currentIdlist: null,
  statusListdAdd: false,
  currentIdcard: null,
  isModalCard: false,
  isModalEmpresas: false,
  estatusAddboard: false,
  rowsBmc: 0,
  rowsTrello: 0,
  userProyect: "",
  accion: "",
  isNav: false,
};
const funcionalidad = (state = estado, action) => {
  switch (action.type) {
    case CURRENT_BOARD_ID: {
      const { boardId } = action.payload;
      return {
        ...state,
        currentIdboard: boardId,
      };
    } /*-CRUD-BOARD------*/
    case STATUS_BOARD_ADD: {
      const { statusBoard } = action.payload;
      return {
        ...state,
        statusBoardAdd: statusBoard,
      };
    } /*-LIST-------------*/
    case CURRENT_LISTS_ID: {
      const { listId } = action.payload;
      return { ...state, currentIdlist: listId };
    } /*-CRUD-LIST--------*/
    case STATUS_LIST_ADD: {
      const { statusListd } = action.payload;
      return { ...state, statusListdAdd: statusListd };
    } /*-CARD--------------*/
    case CURRENT_CARDS_ID: {
      const { cardId } = action.payload;
      return { ...state, currentIdcard: cardId };
    }
    case MODAL_FORM_CARDS: {
      const { ismodal } = action.payload;
      return { ...state, isModalCard: ismodal };
    } /*-BMC-------------*/
    case MODAL_FORM_EMPRESAS: {
      const { ismodal } = action.payload;
      return { ...state, isModalEmpresas: ismodal };
    }
    case ISMENU: {
      const { isnav } = action.payload;
      return { ...state, isNav: isnav };
    }

    case TIPO_ACCION_USUARIO:
      const { userProyect, accion } = action.payload;
      return {
        ...state,
        userProyect: userProyect,
        accion: accion,
      };
    default:
      return state;
  }
};
export default funcionalidad;
