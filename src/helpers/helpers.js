import { format } from "date-fns";
import parseISO from "date-fns/parseISO";
import es from "date-fns/locale/es";
export const validacionLogin = (usuario, clave, toast) => {
  if (usuario.trim().length === 0) {
    toast("El campo de Usuario no debe estar vacio", {
      position: toast.POSITION.BOTTOM_RIGHT,
      className: "foo-bar",
    });

    return false;
  } else if (clave.trim().length === 0) {
    toast("El campo de Clave no debe estar vacio", {
      position: toast.POSITION.BOTTOM_RIGHT,
      className: "foo-bar",
    });

    return false;
  }
  return true;
};
/*-CONFIGURACION-DATATABLE------------------------------*/
export const paginationOptions = {
  rowsPerPageText: "Fila por paginas:",
  rangeSeparatorText: "de",
  noRowsPerPage: true,
  selectAllRowsItem: true,
  selectAllRowsItemText: "Todos",
};
/*-SCRIPT-DETECTA-RUTA-ACTUAL-SI-ES-PAGINA--------------*/
export const infoLocation = (pathname) => {
  let path = [];
  path = pathname.split("/");
  //console.log(path);
  //let pagePath = path.includes("login");
  path.length >= 5
    ? (path = path[path.length - 3])
    : (path = path[path.length - 1]);
  //console.log(path);
  let pagePath = path.split("-").includes("page");
  return {
    path: path,
    pagePath: pagePath,
  };
};
/*-VALIDACION-FILE-------------------------------*/
export const validaFile = (data) => {
  let msg = "";
  let ext = /(.jpg|.jpeg|.png|.gif)$/i;
  if (!ext.exec(data)) {
    msg = "documento";
  } else {
    msg = "imagen";
  }
  return msg;
};
export const isExtension = (cadena) => {
  let separador = ".";
  let division = cadena.split(separador);
  const [, extension] = division;
  return extension;
};
/*-SCRIPT-CHECKLIST------------------------------*/
export const actualizarProgreso = (progreso, data) => {
  let cantidadActual = 0;
  let porcentajePorItem = 0;
  cantidadActual = parseInt(data.length); //1
  porcentajePorItem = 100 / cantidadActual;

  let filtrado = data.filter((item) => item.estado === "activo");

  let porcentajePorItemF = parseFloat(porcentajePorItem);
  let filtradoF = parseInt(filtrado.length);
  let nuevoProgreso = filtradoF * porcentajePorItemF;

  let progresoF = parseInt(progreso);
  let progresF = parseFloat(nuevoProgreso);
  //console.log({ progresoAnterio: progresoF, progresoNuevo: progresF });
  return {
    progresoAnt: progresoF,
    nuevoProgreso: progresF,
  };
};
export const updateProgressDeletItem = (idItem, estado, data, progreso) => {
  let cantidadActual = 0;
  let porcentajePorItem = 0;
  let progresF = 0;
  let nuevo = data.filter((item) => item.id !== idItem);
  cantidadActual = parseInt(nuevo.length);
  porcentajePorItem = 100 / cantidadActual;

  let filtrado = nuevo.filter((item) => item.estado === "activo");
  let porcentajePorItemF = parseFloat(porcentajePorItem);
  let filtradoF = parseInt(filtrado.length);
  let nuevoProgreso = filtradoF * porcentajePorItemF;

  let progresoF = parseInt(progreso);
  progresF = parseFloat(nuevoProgreso);
  //console.log({ progresoAnterio: progresoF, progresoNuevo: progresF });
  return {
    progresoAnt: progresoF,
    nuevoProgreso: progresF,
  };
};
export const formatoFechaView = (date, formato) => {
  if (date === undefined) return null;
  let fechaISO = parseISO(date);
  let fechaFormateda = "";
  fechaFormateda = format(fechaISO, formato, {
    awareOfUnicodeTokens: true,
    locale: es,
  });
  return fechaFormateda;
};

export const viewCardAddDelet = (cantidaActual, tipo) => {
  let total = 0;
  if (tipo === "sumar") {
    total = cantidaActual + 1;
  } else {
    total = cantidaActual - 1;
  }
  return total;
};
